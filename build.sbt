
name := "elasticsearch-swagger"

version := "1.0"

scalaVersion := "2.12.1"

import Dependencies._

lazy val commonSettings = Seq(
  organization := "com.pnmougel",
  version := "0.1.0",
  scalaVersion := "2.12.1"
)
lazy val root = (project in file("."))
  .settings(name := "elasticsearch-swagger")
  .settings(libraryDependencies ++= backendDeps)
  .settings(commonSettings : _*)