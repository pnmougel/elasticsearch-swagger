import sbt._

object Dependencies {
  val scalaCurVersion = "2.12.1"
  val akkaVersion = "2.4.12"
  val akkaHttpVersion = "10.0.0"
  val json4sVersion = "3.5.0"
  //  val elasticVersion = "2.4.1"
  val elastic4sVersion = "5.1.5"
  val log4jVersion = "2.7"
  val circeVersion = "0.6.1"
  val jacksonVersion = "2.8.4"
  val scalaTestVersion = "3.0.1"

  val esVersion = util.Properties.propOrNone("es").getOrElse("6.2.4")

  val backendDeps = Seq(
    // Elasticsearch
    "org.elasticsearch" % "elasticsearch" % esVersion,

    "org.apache.logging.log4j" % "log4j-core" % "2.11.0",
    "org.apache.logging.log4j" % "log4j-api" % "2.11.0",


    // Required to hide a warning at compilation
    "com.vividsolutions" % "jts" % "1.13",

    // Elastic
    "com.sksamuel.elastic4s" %% "elastic4s-core" % elastic4sVersion,

    // Reflection
    "dom4j" % "dom4j" % "1.6.1",
    "com.google.code.gson" % "gson" % "2.3.1",
    "org.reflections" % "reflections" % "0.9.10",

    // Scala compile
    "org.scala-lang" % "scala-compiler" % scalaCurVersion,

    "com.github.therapi" % "therapi-runtime-javadoc" % "0.6.0",
    "com.github.therapi" % "therapi-runtime-javadoc-scribe" % "0.6.0" % "provided",

    // Json4s
    "com.fasterxml.jackson.module" % "jackson-module-scala_2.12" % "2.8.4" force(),

    // Guava
    "com.google.guava" % "guava" % "19.0"
  )
}
