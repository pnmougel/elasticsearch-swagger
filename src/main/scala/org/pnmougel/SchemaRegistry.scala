package org.pnmougel

import java.lang.reflect.{Field, Modifier}

import com.fasterxml.jackson.annotation.JsonIgnore
import com.google.common.base.CaseFormat
import org.elasticsearch.common.ParseField
import org.elasticsearch.index.query.{CommonTermsQueryBuilder, IdsQueryBuilder}
import org.elasticsearch.index.query.functionscore.{ExponentialDecayFunctionBuilder, GaussDecayFunctionBuilder, LinearDecayFunctionBuilder}
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder
import org.elasticsearch.search.suggest.SuggestBuilder
import org.pnmougel.ReflectionUtilities.reflections
import org.pnmougel.builders._
import org.pnmougel.builders.collections.{ArrayPropertyBuilder, CollectionPropertyBuilder, EnumPropertyBuilder, MapParamsPropertyBuilder}
import org.pnmougel.builders.discarded.SupplierPropertyBuilder
import org.pnmougel.builders.geo.GeoPointPropertyBuilder
import org.pnmougel.builders.primitive.{PrimitiveTypePropertyBuilder, PrimitiveValuePropertyBuilder, StringPropertyBuilder}
import org.pnmougel.builders.scores.DecayPropertyBuilder
import org.pnmougel.builders.text.{LocalePropertyBuilder, RegexpPropertyBuilder}
import org.pnmougel.properties._
import org.pnmougel.schemas.{SchemaDefinition, SchemaEnumDefinition, SchemaObjectDefinition, SchemaOneOfDefinition}
import org.reflections.ReflectionUtils._

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.util.control.Exception._
/**
  * Created by nico on 09/06/18.
  */
object SchemaRegistry {
  @JsonIgnore
  val schemaNameToCanonicalName = mutable.HashMap[String, String]()
  val schemas = mutable.LinkedHashMap[String, SchemaDefinition]()

  @JsonIgnore
  val notHandledClasses = mutable.HashSet[Class[_]]()
  @JsonIgnore
  val propertyBuilders = List(
    new PrimitiveTypePropertyBuilder(),
    new PrimitiveValuePropertyBuilder(),
    new StringPropertyBuilder(),

    new ArrayPropertyBuilder(),
    new CollectionPropertyBuilder(),
    new EnumPropertyBuilder(),

    // TO remove and use more specific builder
    new MapParamsPropertyBuilder(),

    new SupplierPropertyBuilder(),
    new CoordinatePropertyBuilder(),
    new GeoPointPropertyBuilder(),
    new TimeValuePropertyBuilder(),
    new FetchSourcePropertyBuilder(),
    new DateTimeZonePropertyBuilder(),
    new RegexpPropertyBuilder(),
    new LocalePropertyBuilder(),

    new XContentPropertyBuilder(),

    // TO remove and use more specific builder
    new ValueSourceConfigPropertyBuilder(),
    // TO remove and use more specific builder
    new ObjectPropertyBuilder()
  )

  var objectCount = 0

  // Register schemas
  DecayPropertyBuilder.registerDecaySchema(classOf[LinearDecayFunctionBuilder])
  DecayPropertyBuilder.registerDecaySchema(classOf[GaussDecayFunctionBuilder])
  DecayPropertyBuilder.registerDecaySchema(classOf[ExponentialDecayFunctionBuilder])


  val fieldNameMapper = Map(
    classOf[SearchSourceBuilder] -> Map(
      "fetchSourceContext" -> "_source",
      "postQueryBuilder" -> "post_filter",
      "indexBoosts" -> "indices_boost",
      "storedFieldsContext" -> "stored_fields",
      "docValueFields" -> "docvalue_fields",
      "sorts" -> "sort"
    ),classOf[Object] -> Map(
      "queryName" -> "_name",
      "fieldName" -> "TODO: FIELD_NAME"
    ),classOf[SuggestBuilder] -> Map(
      "globalText" -> "text",
      "suggestions" -> "TODO: This is a map from a suggestion name to a suggester"
    ),classOf[HighlightBuilder] -> Map(
      "highlighterType" -> "type",
      "boundaryScannerType" -> "boundary_scanner",
      "numOfFragments" -> "number_of_fragments"
    ),classOf[HighlightBuilder.Field] -> Map(
      "highlighterType" -> "type",
      "boundaryScannerType" -> "boundary_scanner",
      "numOfFragments" -> "number_of_fragments"
    ),classOf[IdsQueryBuilder] -> Map(
      "types" -> "type"
    ),classOf[CommonTermsQueryBuilder] -> Map(
      "text" -> "query",
      // Actually it is "minimum_should_match.high_freq" and  "minimum_should_match.low_freq"
      "highFreqMinimumShouldMatch" -> "high_freq",
      "lowFreqMinimumShouldMatch" -> "low_freq"
    )
  )

  def buildObjectSchema(cl: Class[_]): SchemaDefinition = {
    val schemaName = getSchemaName(cl)
    schemas.getOrElse(schemaName, {
      objectCount += 1
      val classFields: List[Field] = allCatch.opt(getAllFields(cl)).map(f => f.toList).getOrElse(List[Field]())

      // Retrieve field names
      val jsonFieldNames = for(field <- classFields;
          if (Modifier.isStatic(field.getModifiers) && field.getType == classOf[ParseField])) yield {
        field.setAccessible(true)
        field.get(null).asInstanceOf[ParseField]
      }


      val schemaDef = new SchemaObjectDefinition(schemaName)
      schemas.put(schemaName, schemaDef)

      var notHandledFieldsForClass = List[Field]()
      var classFieldWithoutJsonMapping = List[Field]()
      var jsonFieldsFound = mutable.HashSet(jsonFieldNames:_*)

      for(field <- classFields) {
        var isJsonMappingForced = false
        val expectedJsonFieldName = (for((classMatcher, fieldMapping) <- fieldNameMapper;
            if(classMatcher.isAssignableFrom(cl));
            (classFieldName, jsonFieldName) <- fieldMapping;
            if(classFieldName == field.getName)) yield {
          isJsonMappingForced = true
          jsonFieldName
        }).headOption.getOrElse({
          val cleanedName = field.getName.replaceAll("Builders?", "")
          CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, cleanedName)
        })

        if(!Modifier.isStatic(field.getModifiers)) {
          // Remove the ParsedField that are matched
          jsonFieldNames.foreach(parsedField => {
            if(parsedField.`match`(expectedJsonFieldName)) {
              jsonFieldsFound.remove(parsedField)
            }
          })

          if(jsonFieldNames.exists(parsedFields => parsedFields.`match`(expectedJsonFieldName)) || isJsonMappingForced) {
            val propertyDefinitionOpt = getPropertyFromField(field, field.getType, expectedJsonFieldName)
            propertyDefinitionOpt.foreach(propertyDefinition => {
              if(!propertyDefinition.isInstanceOf[DiscardedProperty]) {
                schemaDef.addProperty(field.getName, propertyDefinition)
              }

              if(!isPropertyHandled(field, field.getType)) {
                notHandledFieldsForClass :+= field
              }
            })


          } else {
            classFieldWithoutJsonMapping :+= field
          }
        }
      }

//      for(field <- classFields;
//          propertyDefinition <- getPropertyFromField(field, field.getType)) {
//
//        if(!propertyDefinition.isInstanceOf[DiscardedProperty]) {
//          schemaDef.addProperty(field.getName, propertyDefinition)
//        }
//
//        if(!isPropertyHandled(field, field.getType)) {
//          notHandledFieldsForClass :+= field
//        }
//      }

      if(jsonFieldsFound.nonEmpty || notHandledFieldsForClass.nonEmpty || classFieldWithoutJsonMapping.nonEmpty) {
        println(s"\u001B[31m${cl.getCanonicalName}\u001B[0m.(${cl.getSimpleName}.java:20)")
        if(jsonFieldsFound.nonEmpty) {
          println("  Json fields that are not found in class properties")
          println(jsonFieldsFound.map(field => s"  * Field ${field.getPreferredName}").mkString("\n"))

        }
        if(notHandledFieldsForClass.nonEmpty) {
          println("  Field that are not handled")
          println(notHandledFieldsForClass.map(field => s"  * Field ${field.getName} of type ${field.getType}").mkString("\n"))
        }
        if(classFieldWithoutJsonMapping.nonEmpty) {
          println("  Field for which json name is not found")
          println(classFieldWithoutJsonMapping.map(field => s"  * Field ${field.getName} of type ${field.getType}").mkString("\n"))
        }
      }
      schemaDef
    })
  }

  def buildEnumSchema(cl: Class[_]): SchemaDefinition = {
    val schemaName = getSchemaName(cl)
    schemas.getOrElseUpdate(schemaName, new SchemaEnumDefinition(schemaName, cl.getEnumConstants.map(_.toString)))
  }

  def buildOneOfSchema(cl: Class[_]): SchemaDefinition = {
    val schemaName = getSchemaName(cl)
    schemas.getOrElse(schemaName, {
      val schemaDef = new SchemaOneOfDefinition(schemaName)
      schemas.put(schemaName, schemaDef)
      reflections.getSubTypesOf(cl)
        .filter(subClass => !subClass.isInterface && !Modifier.isAbstract(subClass.getModifiers))
        .foreach(subClass => schemaDef.addSchemaRef(buildObjectSchema(subClass).name))
      schemaDef
    })
  }

  def getSchemaName(cl: Class[_]): String = {
    var schemaName = cl.getSimpleName
    var i = 1
    val canonicalName = cl.getCanonicalName
    while(schemaNameToCanonicalName.getOrElse(schemaName, "") == canonicalName) {
      schemaName = s"${cl.getSimpleName}_${i}"
      i += 1
    }
    schemaName = canonicalName
    schemaName
  }

  def getPropertyFromField(field: Field, fieldType: Class[_], fieldName: String): Option[Property] = {
    if (Modifier.isStatic(field.getModifiers)) {
      None
    } else {
      val property = propertyBuilders
        .find(p => p.isMatch(fieldType, field, fieldName))
        .map(p => p.build(fieldType, field, fieldName))
        .getOrElse({
          notHandledClasses.add(fieldType)
          AnyProperty(fieldName, fieldType.getCanonicalName)
        })
      Some(property)
    }
  }

  def isPropertyHandled(field: Field, fieldType: Class[_]): Boolean = {
    var isHandled = true
    if(!Modifier.isStatic(field.getModifiers)) {
      isHandled = propertyBuilders.exists(p => p.isMatch(fieldType, field, field.getName))
    }
    isHandled
  }
}
