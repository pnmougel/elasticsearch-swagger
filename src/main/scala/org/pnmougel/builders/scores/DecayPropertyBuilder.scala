package org.pnmougel.builders.scores

import org.pnmougel.SchemaRegistry.{getSchemaName, schemas}
import org.pnmougel.properties._
import org.pnmougel.properties.fields.FieldProperty
import org.pnmougel.properties.numbers.DoubleProperty
import org.pnmougel.schemas.SchemaObjectDefinition

/**
  * Created by nico on 10/06/18.
  */

object DecayPropertyBuilder {
  def registerDecaySchema(cl: Class[_]): Unit = {
    val schemaName = getSchemaName(cl)
    val schemaDef = new SchemaObjectDefinition(schemaName)
    schemaDef.addProperty("field", FieldProperty("field"))
    schemaDef.addProperty("origin", StringProperty("origin","field_dependent"))
    schemaDef.addProperty("scale", StringProperty("scale", "field_dependent"))
    schemaDef.addProperty("offset", DoubleProperty("offset"))
    schemaDef.addProperty("decay", DoubleProperty("decay"))
    schemas.put(schemaName, schemaDef)
  }
}
