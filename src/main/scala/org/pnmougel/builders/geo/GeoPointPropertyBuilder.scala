package org.pnmougel.builders.geo

import java.lang.reflect.Field

import org.elasticsearch.common.geo.GeoPoint
import org.pnmougel.SchemaRegistry.{getSchemaName, schemas}
import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.properties.numbers.DoubleProperty
import org.pnmougel.properties.Property
import org.pnmougel.properties.references.ObjectRefProperty
import org.pnmougel.schemas.SchemaObjectDefinition

/**
  * Created by nico on 10/06/18.
  */
class GeoPointPropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl == classOf[GeoPoint]

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    val schemaName = getSchemaName(cl)
    val schemaDef = new SchemaObjectDefinition(schemaName)
    schemaDef.addProperty("lat", DoubleProperty("lat"))
    schemaDef.addProperty("lon", DoubleProperty("lon"))
    schemas.put(schemaName, schemaDef)
    ObjectRefProperty(fieldName, schemaName)
  }
}
