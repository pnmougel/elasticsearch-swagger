package org.pnmougel.builders

import java.lang.reflect.Field

import org.joda.time.DateTimeZone
import org.pnmougel.properties._

/**
  * Created by nico on 10/06/18.
  */
class DateTimeZonePropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl == classOf[DateTimeZone]

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    StringProperty(fieldName, "timezone")
//    val schemaName = getSchemaName(cl)
//    val schemaDef = new SchemaObjectDefinition(schemaName)
//    schemaDef.addProperty("time_zone", StringProperty("timezone"))
//    schemas.put(schemaName, schemaDef)
//    RefProperty(schemaName)
  }
}
