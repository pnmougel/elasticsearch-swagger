package org.pnmougel.builders.discarded

import java.lang.reflect.Field

import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.properties.{DiscardedProperty, Property}

class SupplierPropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl == classOf[java.util.function.Supplier[_]]

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
//    val schemaName = getSchemaName(cl)
//    val schemaDef = new SchemaObjectDefinition(schemaName)
//    schemaDef.addProperty("x", DoubleProperty())
//    schemaDef.addProperty("y", DoubleProperty())
//    schemaDef.addProperty("z", DoubleProperty())
//    schemas.put(schemaName, schemaDef)
//    RefProperty(schemaName)
    DiscardedProperty(fieldName)
  }
}

