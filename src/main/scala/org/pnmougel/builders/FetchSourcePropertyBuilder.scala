package org.pnmougel.builders

import java.lang.reflect.Field

import org.elasticsearch.search.fetch.subphase.FetchSourceContext
import org.pnmougel.SchemaRegistry.{getSchemaName, schemas}
import org.pnmougel.properties
import org.pnmougel.properties._
import org.pnmougel.properties.fields.FieldProperty
import org.pnmougel.properties.references.ObjectRefProperty
import org.pnmougel.schemas.SchemaObjectDefinition

/**
  * Created by nico on 10/06/18.
  */
class FetchSourcePropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl == classOf[FetchSourceContext]

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    val schemaName = getSchemaName(cl)
    val schemaDef = new SchemaObjectDefinition(schemaName)
    schemaDef.addProperty("includes", properties.ArrayProperty("includes", FieldProperty("includes")))
    schemaDef.addProperty("excludes", properties.ArrayProperty("excludes", FieldProperty("excludes")))
    schemas.put(schemaName, schemaDef)
    ObjectRefProperty(fieldName, schemaName)
  }
}
