package org.pnmougel.builders.collections

import java.lang.reflect.Field

import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.properties._

/**
  * Created by nico on 10/06/18.
  */
class MapParamsPropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = {
//    field.getName == "params" && cl == classOf[java.util.Map[_,_]]
    cl == classOf[java.util.Map[_,_]]
  }

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    StringProperty(fieldName, "TODO")
//    val schemaName = getSchemaName(cl)
//    val schemaDef = new SchemaObjectDefinition(schemaName)
//    schemaDef.addProperty("time_zone", StringProperty("timezone"))
//    schemas.put(schemaName, schemaDef)
//    RefProperty(schemaName)
  }
}
