package org.pnmougel.builders.collections

import java.lang.reflect.Field

import org.pnmougel.SchemaRegistry
import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.properties.Property
import org.pnmougel.properties.references.{EnumRefProperty, ObjectRefProperty}

/**
  * Created by nico on 10/06/18.
  */
class EnumPropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl.isEnum

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    EnumRefProperty(fieldName, SchemaRegistry.buildEnumSchema(cl).name)
  }
}
