package org.pnmougel.builders.collections

import java.lang.reflect.{Field, ParameterizedType}

import org.pnmougel.SchemaRegistry.getPropertyFromField
import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.properties.{AnyProperty, ArrayProperty, Property}

/**
  * Created by nico on 10/06/18.
  */
class CollectionPropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = {
    classOf[java.util.Collection[_]].isAssignableFrom(cl)
  }

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    (for(genericType <- field.getGenericType.asInstanceOf[ParameterizedType].getActualTypeArguments
          .filter(x => { x.isInstanceOf[Class[_]]})
          .map(x => { x.asInstanceOf[Class[_]] })
          .headOption;
        property <- getPropertyFromField(field, genericType, "no_name_for_array_field")
        ) yield {
      ArrayProperty(field.getName, property)
    }).getOrElse(AnyProperty(fieldName, "invalid_collection"))
  }
}
