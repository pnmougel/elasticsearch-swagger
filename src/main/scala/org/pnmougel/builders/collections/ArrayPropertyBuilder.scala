package org.pnmougel.builders.collections

import java.lang.reflect.Field

import org.pnmougel.SchemaRegistry.getPropertyFromField
import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.properties.{AnyProperty, ArrayProperty, Property}

/**
  * Created by nico on 10/06/18.
  */
class ArrayPropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl.isArray

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    getPropertyFromField(field, cl.getComponentType, "no_name_for_array_field").map(arrayPropertyType => {
      ArrayProperty(fieldName, arrayPropertyType)
    }).getOrElse({
      println("Unable to map array")
      println(cl.getComponentType)
      AnyProperty(fieldName, "invalid_array")
    })
  }
}
