package org.pnmougel.builders

import java.lang.reflect.Field

import org.elasticsearch.common.geo.GeoPoint
import org.elasticsearch.common.unit.TimeValue
import org.pnmougel.SchemaRegistry.{getPropertyFromField, getSchemaName, schemas}
import org.pnmougel.properties.numbers.DoubleProperty
import org.pnmougel.properties._
import org.pnmougel.properties.references.ObjectRefProperty
import org.pnmougel.schemas.SchemaObjectDefinition

/**
  * Created by nico on 10/06/18.
  */
class TimeValuePropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl == classOf[TimeValue]

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    val schemaName = getSchemaName(cl)
    val schemaDef = new SchemaObjectDefinition(schemaName)
    schemaDef.addProperty("duration", DoubleProperty("duration"))
    // TODO: Set the full list of values
    schemaDef.addProperty("unit", OneOfProperty("unit", List("d", "h", "m", "s")))
    schemas.put(schemaName, schemaDef)
    ObjectRefProperty(fieldName, schemaName)
  }
}
