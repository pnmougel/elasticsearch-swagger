package org.pnmougel.builders.text

import java.lang.reflect.Field

import org.apache.lucene.util.automaton.RegExp
import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.properties._

/**
  * Created by nico on 10/06/18.
  */
class LocalePropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl == classOf[java.util.Locale]

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    StringProperty(fieldName, "locale")
  }
}
