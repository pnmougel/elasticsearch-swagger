package org.pnmougel.builders.text

import java.lang.reflect.Field

import org.apache.lucene.util.automaton.RegExp
import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.properties._
/**
  * Created by nico on 10/06/18.
  */
class RegexpPropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl == classOf[RegExp]

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    StringProperty(fieldName, "regexp")
  }
}
