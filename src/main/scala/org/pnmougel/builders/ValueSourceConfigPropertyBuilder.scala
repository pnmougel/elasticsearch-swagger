package org.pnmougel.builders

import java.lang.reflect.Field

import org.elasticsearch.search.aggregations.support.ValuesSourceConfig
import org.pnmougel.properties._

/**
  * Created by nico on 10/06/18.
  */
class ValueSourceConfigPropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl == classOf[ValuesSourceConfig[_]]

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    StringProperty(fieldName, "ValuesSourceConfig TODO")
  }
}
