package org.pnmougel.builders

import java.lang.reflect.{Field, Modifier}

import org.elasticsearch.common.xcontent.ToXContent
import org.pnmougel.SchemaRegistry
import org.pnmougel.properties.Property
import org.pnmougel.properties.references.{MultiObjectRefProperty, ObjectRefProperty}

/**
  * Created by nico on 10/06/18.
  */
class XContentPropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = classOf[ToXContent].isAssignableFrom(cl)

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    if(cl.isInterface || Modifier.isAbstract(cl.getModifiers)) {
      MultiObjectRefProperty(fieldName, SchemaRegistry.buildOneOfSchema(cl).name)
    } else {
      ObjectRefProperty(fieldName, SchemaRegistry.buildObjectSchema(cl).name)
    }
  }
}
