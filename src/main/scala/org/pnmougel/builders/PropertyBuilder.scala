package org.pnmougel.builders

import java.lang.reflect.Field

import org.pnmougel.properties.Property

/**
  * Created by nico on 10/06/18.
  */
abstract class PropertyBuilder {
  def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean

  def build(cl: Class[_], field: Field, fieldName: String): Property
}
