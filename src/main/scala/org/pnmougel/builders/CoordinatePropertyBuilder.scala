package org.pnmougel.builders

import com.vividsolutions.jts.geom.Coordinate
import org.pnmougel.SchemaRegistry.{getSchemaName, schemas}
import org.pnmougel.properties.numbers.DoubleProperty
import org.pnmougel.properties.Property
import org.pnmougel.schemas.SchemaObjectDefinition
import java.lang.reflect.Field

import org.pnmougel.properties.references.ObjectRefProperty

class CoordinatePropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl == classOf[Coordinate]

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    val schemaName = getSchemaName(cl)
    val schemaDef = new SchemaObjectDefinition(schemaName)
    schemaDef.addProperty("x", new DoubleProperty("x"))
    schemaDef.addProperty("y", new DoubleProperty("y"))
    schemaDef.addProperty("z", new DoubleProperty("z"))
    schemas.put(schemaName, schemaDef)
    ObjectRefProperty(fieldName, schemaName)
  }
}

