package org.pnmougel.builders

import java.lang.reflect.Field

import org.pnmougel.properties.{AnyProperty, Property}

/**
  * Created by nico on 10/06/18.
  */
class ObjectPropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl == classOf[java.lang.Object]

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    AnyProperty(fieldName, "string_object")
  }
}
