package org.pnmougel.builders.primitive

import java.lang.reflect.Field

import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.properties.{AnyProperty, Property}
import org.pnmougel.properties.numbers.{DoubleProperty, FloatProperty, IntegerProperty, LongProperty}

import scala.collection.mutable

/**
  * Created by nico on 10/06/18.
  */
class PrimitiveTypePropertyBuilder extends PropertyBuilder {
  private val primitiveTypeToClass = mutable.HashMap[String, String => Property](
    "boolean" -> (n => AnyProperty(n, "boolean")),
    "byte" -> (n => AnyProperty(n, "byte")),
    "char" -> (n => AnyProperty(n, "char")),
    "short" -> (n => AnyProperty(n, "short")),
    "int" -> (n => IntegerProperty(n)),
    "long" -> (n => LongProperty(n)),
    "float" -> (n => FloatProperty(n)),
    "double" -> (n => DoubleProperty(n))
  )

  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = cl.isPrimitive

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    primitiveTypeToClass(cl.getTypeName)(fieldName)
  }
}
