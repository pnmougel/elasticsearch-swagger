package org.pnmougel.builders.primitive

import java.lang.reflect.Field

import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.properties.{Property, StringProperty}

/**
  * Created by nico on 10/06/18.
  */
class StringPropertyBuilder extends PropertyBuilder {
  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = classOf[String].isAssignableFrom(cl)

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    StringProperty(fieldName, "string")
  }
}
