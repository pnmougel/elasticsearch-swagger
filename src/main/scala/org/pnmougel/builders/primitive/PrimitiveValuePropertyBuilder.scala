package org.pnmougel.builders.primitive

import java.lang.reflect.Field

import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.properties.{AnyProperty, Property}
import org.pnmougel.properties.numbers.{DoubleProperty, FloatProperty, IntegerProperty, LongProperty}

import scala.collection.mutable

/**
  * Created by nico on 10/06/18.
  */
class PrimitiveValuePropertyBuilder extends PropertyBuilder {

  private val typeToProperty = mutable.HashMap[Class[_], String => Property](
    classOf[java.lang.Boolean] -> (n => AnyProperty(n, "boolean")),
    classOf[java.lang.Byte] -> (n => AnyProperty(n, "byte")),
    classOf[java.lang.Short] -> (n => AnyProperty(n, "short")),
    classOf[java.lang.Integer] -> (n => IntegerProperty(n)),
    classOf[java.lang.Long] -> (n => LongProperty(n)),
    classOf[java.lang.Float] -> (n => FloatProperty(n)),
    classOf[java.lang.Double] -> (n => DoubleProperty(n))
  )

  override def isMatch(cl: Class[_], field: Field, fieldName: String): Boolean = typeToProperty.contains(cl)

  override def build(cl: Class[_], field: Field, fieldName: String): Property = {
    typeToProperty(cl)(fieldName)
  }
}
