package org.pnmougel.properties.fields

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.pnmougel.properties.Property

/**
  * Created by nico on 09/06/18.
  */
@JsonIgnoreProperties(Array("itemsType", "schemas", "schemaName", "_name", "_format"))
case class FieldProperty(_name: String) extends Property(_name, "field") {
  val `type` = "string"
}
