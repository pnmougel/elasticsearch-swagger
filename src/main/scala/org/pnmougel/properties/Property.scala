package org.pnmougel.properties

import com.fasterxml.jackson.annotation.{JsonIgnoreProperties, JsonProperty}

/**
  * Created by nico on 09/06/18.
  */
@JsonIgnoreProperties(Array("_name", "_format"))
class Property(@JsonProperty val name: String,
               @JsonProperty val format: String) {

}

case class AnyProperty(_name: String, _format: String) extends Property(_name, _format)