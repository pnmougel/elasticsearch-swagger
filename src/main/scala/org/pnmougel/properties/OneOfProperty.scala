package org.pnmougel.properties

import com.fasterxml.jackson.annotation.{JsonIgnore, JsonIgnoreProperties, JsonProperty}

/**
  * Created by nico on 09/06/18.
  */
@JsonIgnoreProperties(Array("schemas", "schemaName", "_name", "_format"))
case class OneOfProperty(_name: String, schemas: Seq[String]) extends Property(_name, "one_of") {
  @JsonProperty
  val oneOf: Seq[OneOfRefProperty] = schemas.map(schemaName => OneOfRefProperty(schemaName))
}

