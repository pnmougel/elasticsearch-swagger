package org.pnmougel.properties

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
  * Created by nico on 09/06/18.
  */
@JsonIgnoreProperties(Array("schemaName", "_name", "_format"))
case class StringProperty(_name: String, val _format: String) extends Property(_name, _format) {
  val `type` = "String"
}
