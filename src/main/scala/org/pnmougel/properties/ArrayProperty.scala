package org.pnmougel.properties

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
  * Created by nico on 09/06/18.
  */
@JsonIgnoreProperties(Array("itemsType", "schemas", "schemaName", "_name", "_format"))
case class ArrayProperty(_name: String, itemsType: Property) extends Property(_name, "array") {
  val `type` = "array"
  val items = itemsType
}
