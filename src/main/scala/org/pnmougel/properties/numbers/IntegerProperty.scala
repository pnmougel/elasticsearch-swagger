package org.pnmougel.properties.numbers

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.pnmougel.properties.Property

/**
  * Created by nico on 09/06/18.
  */
@JsonIgnoreProperties(Array("itemsType", "schemas", "schemaName", "_name", "_format"))
case class IntegerProperty(_name: String) extends Property(_name, "int32") {
  val `type` = "integer"
}
