package org.pnmougel.properties.references

import com.fasterxml.jackson.annotation.{JsonIgnore, JsonIgnoreProperties, JsonProperty}
import org.pnmougel.properties.Property

/**
  * Created by nico on 09/06/18.
  */
@JsonIgnoreProperties(Array("schemaName", "_name", "_format"))
case class EnumRefProperty(_name: String, @JsonIgnore schemaName: String) extends Property(_name, "enum_reference") {
  @JsonProperty
  val `$ref` = s"#/components/schemas/$schemaName"
}
