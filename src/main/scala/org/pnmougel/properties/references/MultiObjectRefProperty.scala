package org.pnmougel.properties.references

import com.fasterxml.jackson.annotation.{JsonIgnore, JsonIgnoreProperties, JsonProperty}
import org.pnmougel.properties.Property

@JsonIgnoreProperties(Array("schemaName", "_name", "_format"))
case class MultiObjectRefProperty(_name: String, @JsonIgnore schemaName: String) extends Property(_name, "multi_object_reference") {
  @JsonProperty
  val `$ref` = s"#/components/schemas/$schemaName"
}