package org.pnmougel.properties

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
  * Created by nico on 09/06/18.
  */
@JsonIgnoreProperties(Array("schemas", "schemaName", "_name", "_format"))
case class DiscardedProperty(_name: String) extends Property(_name, "ignore") {
}
