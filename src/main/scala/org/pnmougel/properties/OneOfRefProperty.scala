package org.pnmougel.properties

import com.fasterxml.jackson.annotation.{JsonIgnore, JsonIgnoreProperties, JsonProperty}

@JsonIgnoreProperties(Array("schemas", "schemaName", "_name", "_format"))
case class OneOfRefProperty(@JsonIgnore schemaName: String) {
  @JsonProperty
  val `$ref` = s"#/components/schemas/$schemaName"
}