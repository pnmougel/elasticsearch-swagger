package org.pnmougel.swagger

import org.pnmougel.SchemaRegistry

/**
  * Created by nico on 10/06/18.
  */
case class SwaggerDoc() {
  val openapi = "3.0.0."
  val components = Map("schemas" -> SchemaRegistry.schemas)
}
