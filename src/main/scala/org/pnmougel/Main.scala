package org.pnmougel

import java.io.{File, PrintWriter}
import java.util.logging.LogManager

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.dataformat.yaml.{YAMLFactory, YAMLGenerator}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.elasticsearch.index.query.CommonTermsQueryBuilder
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.pnmougel.ReflectionUtilities.{esVersion, reflectionFile, reflections, serializer}
import org.pnmougel.builders.PropertyBuilder
import org.pnmougel.swagger.SwaggerDoc
import org.reflections.Reflections
import org.reflections.util.FilterBuilder

import scala.reflect.ClassTag


/**
  * Created by nico on 09/06/18.
  */
object Main {
  def main(args: Array[String]): Unit = {
    val esVersion = util.Properties.propOrNone("es").getOrElse("6.2.4")

//    var reflections: Reflections = new Reflections("org.pnmougel")
//    reflections.getSubTypesOf(classOf[PropertyBuilder]).forEach(f => {
//      println(f)
//      // println(f.newInstance())
//    })

    val jsonMapper = getMapper(false)
    
     SchemaRegistry.buildObjectSchema(classOf[SearchSourceBuilder])
//    SchemaRegistry.buildObjectSchema(classOf[CommonTermsQueryBuilder])

    println(SchemaRegistry.notHandledClasses.mkString("\n"))
    println(s"${SchemaRegistry.notHandledClasses.size} not handled properties")

    val baseSchemaDir = new File("org/pnmougel/schemas")
    if(!baseSchemaDir.exists()) {
      baseSchemaDir.mkdir()
    }

    val jsonOutput = new PrintWriter(s"schemas/schema_${esVersion}.json")
    jsonOutput.println(jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(SwaggerDoc()))
    jsonOutput.flush()
    jsonOutput.close()
    
    println(SchemaRegistry.objectCount + " objects")
  }

  def getMapper(useYml: Boolean): ObjectMapper = {
    val mapper = new ObjectMapper() with ScalaObjectMapper
    mapper.registerModule(DefaultScalaModule)
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
    mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
    mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
    mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true)
    mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, true)
    mapper.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, false)
    mapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)

    mapper
  }
}
