package org.pnmougel

import java.io.File

import org.reflections.Reflections
import org.reflections.serializers.JsonSerializer
import org.reflections.util.FilterBuilder

import scala.collection.JavaConversions._
import scala.reflect.ClassTag
import scala.reflect.runtime._
import scala.reflect.runtime.universe._
import scala.tools.reflect.ToolBox
import scala.util.control.Exception._


/**
  * Created by nico on 06/10/16.
  */
object ReflectionUtilities {
  val esVersion = util.Properties.propOrNone("es").getOrElse("6.2.4")

  val reflectionPath = s"META-INF/reflections/reflections_${esVersion}.json"
  val serializer: JsonSerializer = new JsonSerializer()
  val reflectionFile = new File("target/scala-2.12/classes/" + reflectionPath)
  var reflections: Reflections = null
  if(reflectionFile.exists()) {
    reflections = Reflections.collect("META-INF/reflections", new FilterBuilder().include(s"reflections_${esVersion}.json"), serializer)
  } else {
    reflections = new Reflections("org.elasticsearch")
    reflections.save("target/scala-2.12/classes/" + reflectionPath, serializer)
  }

  def getFieldsDefaultParameters[T: TypeTag]: Map[String, Any] = {
    val classTag = ClassTag[T](typeTag[T].mirror.runtimeClass(typeTag[T].tpe))
    val mod = currentMirror.classSymbol(classTag.runtimeClass).companion.asModule
    val im = currentMirror.reflect(currentMirror.reflectModule(mod).instance)
    val ts = im.symbol.typeSignature
    val mApply = ts.member(universe.TermName("apply")).asMethod
    Map(mApply.paramLists.flatten.zipWithIndex.map { case (p, i) =>
      (p.name.toString, ts.member(universe.TermName(s"apply$$default$$${i + 1}")))
    }.filter(_._2.isMethod).map(m => {
      (m._1, im.reflectMethod(m._2.asMethod)())
    }): _*)
  }

  def newInstanceOf[T](classOf: Class[_ <: T]): Option[T] = {
    (for (constructor <- classOf.getConstructors
          if constructor.getParameterCount == 0) yield {
      constructor.setAccessible(true)
      constructor.newInstance().asInstanceOf[T]
    }).headOption
  }

  def newInstanceOfUntyped(classOf: Class[_]): Option[_] = {
    (for (constructor <- classOf.getConstructors
          if constructor.getParameterCount == 0) yield {
      constructor.setAccessible(true)
      constructor.newInstance()
    }).headOption
  }

  /**
    * Retrieve all the instances of type T
    *
    * @param tag
    * @tparam T a class
    * @return
    */
  def getInstancesOf[T](implicit tag: ClassTag[T]): Array[T] = {
    val subTypes = reflections.getSubTypesOf(tag.runtimeClass)
    (for (subType <- subTypes.toList;
          constructor <- subType.getDeclaredConstructors
          if constructor.getParameterCount == 0
    ) yield {
      constructor.setAccessible(true)
      allCatch.opt {
        constructor.newInstance().asInstanceOf[T]
      }
    }).flatten.toArray
  }

  def getSubTypesOf[T](implicit tag: ClassTag[T]): List[Type] = {
    val subTypes = reflections.getSubTypesOf(tag.runtimeClass)
    for (subType <- subTypes.toList) yield {
      runtimeMirror(subType.getClassLoader).classSymbol(subType).toType
    }
  }

  def getSubClassesOf[T: TypeTag](implicit tag: ClassTag[T]): List[Class[_]] = {
    reflections.getSubTypesOf(tag.runtimeClass).toList
  }

  def annotationsOf[T: universe.TypeTag, A: universe.TypeTag]: List[(Symbol, Option[A])] = {
    annotationsOf[A](symbolOf[T].asClass)
  }

  def annotationsOf[A: universe.TypeTag](classSymbol: ClassSymbol): List[(Symbol, Option[A])] = {
    classSymbol.primaryConstructor.typeSignature.paramLists.flatMap { fields =>
      fields.map { field =>
        val annotation: Option[Annotation] = field.annotations.find(_.tree.tpe =:= universe.typeOf[A])
        val annotationInstance = annotation.map { a =>
          buildAnnotationInstance[A](a)
        }
        (field, annotationInstance)
      }
    }
  }

  def instanceOf[T: TypeTag](args: AnyRef*) = {
    val cl = symbolOf[T].asClass
    currentMirror.reflectClass(cl).reflectConstructor(cl.primaryConstructor.asMethod)(args: _*).asInstanceOf[T]
  }

  def methodsAnnotationsOf[T: universe.TypeTag, A: universe.TypeTag]: Iterable[(MethodSymbol, Option[A])] = {
    val methods: Iterable[MethodSymbol] = typeOf[T].decls.collect { case m: MethodSymbol => m }
    methods.map { method =>
      val annotation: Option[Annotation] = method.annotations.find(_.tree.tpe =:= universe.typeOf[A])
      val annotationInstance = annotation.map { annotation =>
        buildAnnotationInstance[A](annotation)
      }
      (method, annotationInstance)
    }
  }
  val toolbox = currentMirror.mkToolBox()
  def buildAnnotationInstance[T](annotation: Annotation): T = {
    toolbox.eval(toolbox.untypecheck(annotation.tree)).asInstanceOf[T]
  }

  def classToClassSymbol(cl: Class[_]): ClassSymbol = {
    val mirror = runtimeMirror(cl.getClassLoader)
    mirror.classSymbol(cl)
  }

  def isOptionType(t: Type): Boolean = t <:< typeOf[Option[_]]

  def isEnumerationType(t: Type): Boolean = t.asInstanceOf[TypeRef].pre <:< typeOf[Enumeration]

  def isCollectionType(t: Type): Boolean = t <:< typeOf[Array[_]] || t <:< typeOf[Seq[_]] || t <:< typeOf[Traversable[_]]

  def getEnumerationValues(t: Type): Array[String] = {
    val parent = t.asInstanceOf[TypeRef].pre
    val module = currentMirror.staticModule(parent.typeSymbol.fullName)
    val obj = currentMirror.reflectModule(module)
    val enum = obj.instance.asInstanceOf[Enumeration]
    enum.values.toArray.map(_.toString)
  }


  /**
    * Check if typeToCheck is either T or has type args of T
    * @param typeToCheck
    * @tparam T
    * @return
    */
  def isOfType[T : TypeTag](typeToCheck: Type): Boolean = {
    val t = typeOf[T]
    t =:= typeToCheck || typeToCheck.typeArgs.exists(_ =:= t)
  }
}
