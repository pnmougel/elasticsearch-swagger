package org.pnmougel.schemas

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.pnmougel.properties.Property

import scala.collection.mutable

/**
  * Created by nico on 09/06/18.
  */

@JsonIgnoreProperties(Array("name"))
class SchemaObjectDefinition(name: String) extends SchemaDefinition(name) {
  val `type` = "object"
  var properties = mutable.LinkedHashMap[String, Property]()

  def addProperty(name: String, property: Property): Unit = {
    properties.put(name, property)
  }
}

