package org.pnmougel.schemas

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.sksamuel.elastic4s.searches.queries.HasChildQueryDefinition

/**
  * Created by nico on 10/06/18.
  */
@JsonIgnoreProperties(Array("name"))
class SchemaDefinition(val name: String) {
  HasChildQueryDefinition
}
