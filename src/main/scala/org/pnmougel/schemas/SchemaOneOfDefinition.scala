package org.pnmougel.schemas

import com.fasterxml.jackson.annotation.{JsonIgnoreProperties, JsonProperty}
import org.pnmougel.properties.OneOfRefProperty

/**
  * Created by nico on 10/06/18.
  */
@JsonIgnoreProperties(Array("name"))
class SchemaOneOfDefinition(name: String) extends SchemaDefinition(name) {
  var oneOf = List[OneOfRefProperty]()
  val `format` = "oneOf"

  def addSchemaRef(ref: String): Unit = {
    oneOf :+= OneOfRefProperty(ref)
  }
}
