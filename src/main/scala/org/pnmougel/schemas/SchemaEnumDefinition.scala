package org.pnmougel.schemas

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
  * Created by nico on 10/06/18.
  */
@JsonIgnoreProperties(Array("name", "values"))
class SchemaEnumDefinition(name: String, values: Array[String]) extends SchemaDefinition(name) {
  val `type` = "string"
  val `format` = "enum"
  val enum = values
}
